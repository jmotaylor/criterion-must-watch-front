# Criterion Must Watch - Front

This is the front end of the Criterion Must Watch project written in Vue 3. The original was written in Node.js, and I am converting it to Vue to get some practice for my next job, which I'm starting in November.

The Criterion Must Watch project is an app that helps you decide which of the films leaving the Criterion Channel at the end of the month you should watch next by showing you popular films that are leaving, all-time-favorite films that are leaving, and a list of leving films with reviews from Rotten Tomatoes sorted from highest to lowest.

## Technologies

* Vue 3.2
* TypeScript 4.6
* Tailwind 3.1