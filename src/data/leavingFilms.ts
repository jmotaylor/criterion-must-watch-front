import { FilmType } from "../enums";
import Leaving from "../types/films/Leaving";


export const leavingFilms: Leaving[] = [
  {
    title: 'Touch of Evil',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/touch-of-evil',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/f7bbbee4-4be3-40d2-b300-9b5a7aa58e4c.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'Arabesque',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/arabesque',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/edc70368-5371-4dcc-a554-7e155689d25a.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'Experiment in Terror',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/experiment-in-terror',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/4ab078bb-bfdb-48dc-ab60-3466014f8a23.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'The Asphalt Jungle',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/the-asphalt-jungle',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/20975bf9-7cb0-4194-b307-5641e57befdf.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'Charade',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/charade',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/6d7595bf-ff0a-4dca-a13e-5b911d01239b.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'I Love You Again',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/i-love-you-again',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/8886deb9-8c95-4364-b669-a30267d4f32e-acd73f42.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'Lost Horizon',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/lost-horizon',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/a4b8a4f5-c12a-4c68-9b53-e4e11f0502dc.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  },
  {
    title: 'Two for the Road',
    link: 'https://www.criterionchannel.com/leaving-october-31/videos/two-for-the-road',
    image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/f222ff43-e593-46cc-8c36-856ab2105124.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
    type: FilmType.LEAVING
  }
]

// LeavingFilm {
//   title: 'Touch of Evil',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/touch-of-evil',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/f7bbbee4-4be3-40d2-b300-9b5a7aa58e4c.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Arabesque',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/arabesque',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/edc70368-5371-4dcc-a554-7e155689d25a.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Experiment in Terror',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/experiment-in-terror',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/4ab078bb-bfdb-48dc-ab60-3466014f8a23.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {

//   review: undefined,
//   type: 0
// },
// LeavingFilm {

//   review: undefined,
//   type: 0
// },
// LeavingFilm {

//   review: undefined,
//   type: 0
// },
// LeavingFilm {

//   review: undefined,
//   type: 0
// },
// LeavingFilm {

//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Penthouse',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/penthouse',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/e5d4cbb8-abed-4d4e-bca9-a1c02831a23b.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Mr. Hobbs Takes a Vacation',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/mr-hobbs-takes-a-vacation',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/69c6c4f6-ad8a-464c-8c52-1e641acd61d9.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Tape',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/tape',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/3266d392-e000-4206-b53b-4b1be9ba0e80.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Better Luck Tomorrow',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/better-luck-tomorrow',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/48dbe383-9e90-46c9-b9a1-a2222d4f4133.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Husbands',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/husbands',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/cc9fec54-a6b3-433b-89a5-477eb0e5e3d6.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'The Great Race',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/the-great-race',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/7e62c871-4b61-44d6-9e99-b038858c63e9.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Stamboul Quest',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/stamboul-quest',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/c11f45ab-5a84-4c92-b7a9-08f66c84596f-a530d6be.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },
// LeavingFilm {
//   title: 'Love Me Tonight',
//   link: 'https://www.criterionchannel.com/leaving-october-31/videos/love-me-tonight',
//   image: 'https://vhx.imgix.net/criterionchannelchartersu/assets/6c39afc1-ac7a-4919-850b-60c402b0ae70.jpg?auto=format%2Ccompress&fit=crop&h=360&q=70&w=640',
//   review: undefined,
//   type: 0
// },