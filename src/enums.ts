enum FilmType {
  LEAVING,
  POPULARNOW,
  ALLTIMEFAVORITES,
  REVIEWS
}

export { FilmType };