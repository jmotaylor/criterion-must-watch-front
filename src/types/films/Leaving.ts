import Film from "./Film";
import { FilmType } from "../../enums";
import { Rotten } from "./Rotten";

export default class Leaving extends Film {
  constructor(
    public title: string,
    public link?: string,
    public image?: string,
    public rotten?: Rotten,
    public type: FilmType = FilmType.LEAVING
  ) {
    super(title, link, image);
  }
}