import { FilmType } from "../../enums";

export class Rotten {
  constructor(
    public title: string,
    public link?: string,
    public percentage?: number | null,
    public freshness?: string,
    public type: FilmType = FilmType.REVIEWS
  ) {}
}