export default class Film {
  constructor(
    public title: string,
    public link?: string,
    public image?: string,
    // public review?: Review 
  ) {}
}